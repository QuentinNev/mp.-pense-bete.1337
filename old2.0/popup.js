// React to user actions

let btnSet = document.getElementById("set")
let btnGet = document.getElementById("get")
let btnInject = document.getElementById("inject")
let btnCall = document.getElementById("call")

function getNotes() {
  chrome.runtime.sendMessage({type: "get"}, function(response) {
    return response
  })
}

function setNote(newNote) {
  chrome.runtime.sendMessage({type: "set", note: newNote})
}

btnSet.onclick = function() {
  console.log("Set")
}

btnGet.onclick = function() {
  console.log("Get")
}

btnInject.onclick = function() {
  chrome.runtime.sendMessage({type: "inject"})
}

btnCall.onclick = function() {
  chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
    chrome.tabs.sendMessage(tabs[0].id, {type: "call"})
  })
}