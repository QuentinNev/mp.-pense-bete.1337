// Storage manager

function getNotes() {
  return chrome.storage.sync.get("notes"), function () {
    console.log("Got notes !")
  }
}

function setNote(newNote) {
  var notes = []
  notes = getNotes()

  notes.map(note => note = (note.name === newNote.name) ? newNote : note)
  chrome.storage.sync.set({ "notes": notes }), function () {
    console.log("Notes are set !")
  }
}

function injectJs() {
  chrome.tabs.executeScript({ file: 'contentScript.js' })
}

chrome.runtime.onStartup.addListener(
  function () {
    chrome.pageAction.show(1)
    console.log(";MDRERKNERLKSDFLK")
  }
)

chrome.runtime.onMessage.addListener(
  function (request, sender, sendResponse) {
    switch (request.type) {
      case "get":
        sendResponse(getNotes())
        break
      case "set":
        setNote(request.note)
        break
      case "inject":
        injectJs()
        break
      default:
        break
    }
  }
)