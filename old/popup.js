//#region js version

let step1 = document.getElementById('step1')
let step2 = document.getElementById('step2')
let step3 = document.getElementById('step3')
console.log(step3)

function getNotes() {
  return chrome.storage.sync.get("notes")
}

function setNote(newNote) {
  var notes = []
  notes = chrome.storage.sync.get("notes")

  notes.map(note => note = (note.name === newNote.name) ? newNote : note)
  chrome.storage.sync.set({"notes": notes}), function() {
    console.log("Notes are set !")
  }
}

function setFirstNote() {
  var notes = [
    {
      name: "Valaren",
      note: "Oui"
    }
  ]
  console.log("First note is set !")

  chrome.storage.sync.set({"notes": notes}), function() {
    console.log("First note is set !")
  }
}

step1.onclick = function() {
  getNotes()
}

var fakeNote = {
  name: "Valaren",
  note: "C'est un vrai connard !"
}

step2.onclick = function() {
  setNote(fakeNote)
}

step3.onclick = function() {
  console.log("First note is set !")
  setFirstNote()
}

//#endregion