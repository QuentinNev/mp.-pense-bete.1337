// Called when the user clicks on the browser action.
chrome.browserAction.onClicked.addListener(function (tab) {
  chrome.tabs.executeScript({
    file: 'jquery-3.3.1.min.js'
  }, function () {
    chrome.tabs.executeScript({
      code: '(' + function (params) {
        // Code here is executed in FA's DOM context
        // It can't access anything outside

        var toolTipPosition = {
          size: {
            x: 350,
            y: 500
          },
          offset: {
            x: - 200/2,
            y: 500 + 10
          }
        }

        var toolTipStyle = {
          width: toolTipPosition.size.x,
          height: toolTipPosition.size.y,
          backgroundColor: '#ecf0f1',
          position: 'absolute',
          zIndex: 99,
          borderRadius: 10,
          border: '5px solid',
          borderColor: '#057108',

        }

        // Highlight occurences and add class
        $(".message-placeholder-contenu").html(function (index, html) {
          return html.replace(/Valaren/g, "<b class='Valaren'>Valaren</b>")
        })

        // Show toolTip
        $(".Valaren").mouseenter(function (e) {
          $('#toolTip').css({
            left: e.pageX + toolTipPosition.offset.x,
            top: e.pageY - toolTipPosition.offset.y,
            display: 'block',
            ...toolTipStyle
          })
        })

        // Hide toolTip
        $(".Valaren").mouseleave(function (e) {
          $('#toolTip').css({
            left: e.pageX + toolTipPosition.offset.x,
            top: e.pageY - toolTipPosition.offset.y,
            display: 'none',
            ...toolTipStyle
          })
        })

        // Create toolTip and add an id
        var toolTip = document.createElement("div")
        var toolTipContent = document.createElement("div")
        toolTip.id = "toolTip"
        toolTipContent.id = "toolTipContent"

        $("body").append(toolTip)
        $("#toolTip").append(toolTipContent)
        $("#toolTipContent").css({
          margin: 5
        })

        // Set toolTip text
        $("#toolTipContent").text("CACA")

        return { success: true, html: document.body.innerHTML };
      } + ')()'
    })
  })
})
