# +mp.#Pense-bête.1337

Extension Chrome pour le jeu de rôle [Odyssée](https://jdr-odyssee.net/)

Offre aux joueurs la possibilité d'ajouter une note lié à un nom pour qu'ils puissent consulter ces informations simplement en survolant le nom dans le FA avec sa souris (pour peu qu'il soit correctement orthographié).

## Fonctionnalités implémentées

- Ajout de note
  - Liée directement au nom du personnage
    - Si déjà existant, remplace la note existante
- Affichage des notes dans la popup de l'extension
  - Cliquer sur une note permet de remplir les champs pour en créer une, permettant de la modifier
- Rafraîchissement manuel des notes
- Vider les notes *debug_feature*

## Améliorations possibles

1. Ajout d'une note liée à un nom
1. Mise en évidence des noms possédant une note
1. Consultation de la note au survol du nom mis en évidence
1. Couleurs pour les noms mit en évidence
1. Notes avancées
     1. Plusieurs notes par personnage
     1. Plusieurs noms par personnage
     1. Composants de base
          1. Tableau, liste à puce, lien hypertext
1. Notes liées au FA (un PJ et PNJ ne partagent pas leurs "pensées", donc leurs notes)
1. Exportation des notes (quand un PNJ change de mains)
1. Sauvegarde des données sur le cloud
1. Ajouter un méchant virus pour pirater toutes les CB
     1. S'enfuir en slip en courant sur la musique de Benny Hills à travers les rues de la Baie