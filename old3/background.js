//#region Active on localhost or jdr-odyssee.net

var rule = {
  conditions: [
    new chrome.declarativeContent.PageStateMatcher({
      pageUrl: { hostSuffix: 'jdr-odysse.net' }
    }),
    new chrome.declarativeContent.PageStateMatcher({
      pageUrl: { hostSuffix: '127.0.0.1' }
    })
  ],
  actions: [new chrome.declarativeContent.ShowPageAction()]
}

chrome.runtime.onInstalled.addListener(function (details) {
  chrome.declarativeContent.onPageChanged.removeRules(undefined, function () {
    chrome.declarativeContent.onPageChanged.addRules([rule])
  })
})

//#endregion

//#region Data provider

let newNotes = []


function getNotes() {
  chrome.storage.sync.get('notes', function (data) { newNotes = data.notes })
}



async function setNote(newNote) {
  getNotes()
  chrome.storage.sync.set({ notes: [newNote] }, function () { })

  /*
  notes = getNotes()
  console.log(notes)
  if (!notes) newNotes = [newNote]
  else newNotes = notes.map(note => note = (note.name === newNote.name) ? newNote : note)
  console.log(newNotes)

  chrome.storage.sync.set({ notes: newNotes }, function () {
    if (chrome.runtime.error) console.log("FUCK SHIT MAN THERE IS AN ERROR")
    else console.log("Notes are set !", newNotes)
  })*/

  /*
  getNotes().then((notes) => {
    console.log("Got notes", mdr)
    if (!notes) {
      notes = [newNote]
    }
    newNotes = notes.map(note => note = (note.name === newNote.name) ? newNote : note)
    chrome.storage.sync.set({ "notes": notes }).then(() => {
      console.log("Notes are set !", notes)
    })
  })*/


}

//#endregion

//#region Message listener / Data provider

chrome.runtime.onMessage.addListener(
  function (message, callback) {
    switch (message.action) {
      case 'set':
        setNote(message.note)
        break
      case 'get':
        getNotes()
        callback
        break
    }
  }
)

//#endregion