// React to user actions

$(document).ready(function () {
  // Inject 
  chrome.runtime.sendMessage({ type: "inject" })

  let btnSet = document.getElementById("set")
  let btnGet = document.getElementById("get")
  let btnInject = document.getElementById("inject")
  let btnCall = document.getElementById("call")

  let noteName = document.getElementById("name")
  let noteContent = document.getElementById("content")

  async function getNotes() {
    chrome.runtime.sendMessage({ action: "get" }, function (response) {
      return response
    })
  }

  function setNote(newNote) {
    chrome.runtime.sendMessage({ action: "set", note: newNote })
  }

  // SET
  btnSet.onclick = function () {
    let note = { name: noteName.value, content: noteContent.value }
    setNote(note)
    //console.log("Set")
  }

  // GET
  btnGet.onclick = async function () {
    console.log("Get")
    let notesDiv = document.getElementById("notes")
    notesDiv.innerText = await getNotes()
  }

  btnInject.onclick = function () {
  }

  btnCall.onclick = function () {
    /*chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
      chrome.tabs.sendMessage(tabs[0].id, { type: "call" })
    })*/
    chrome.runtime.sendMessage({ action: "fdp ! <3" }, (response) => {

    })
  }
})