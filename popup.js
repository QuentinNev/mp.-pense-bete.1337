
// popup.js

document.body.onload = function () {
  refreshNotes()
}

// Set
document.getElementById("set").onclick = async function () {
  let newNoteName = document.getElementById("name").value
  let newNoteContent = document.getElementById("content").value

  chrome.storage.sync.get("notes", function (datas) {
    if (!chrome.runtime.error) {
      let newNote = { name: newNoteName, content: newNoteContent }
      let newNotes = []

      // If notes are empty
      if (datas.notes) {
        // If note already exist
        if (datas.notes.some((note) => note.name == newNote.name)) {
          newNotes = datas.notes.map(note => (note.name == newNote.name) ? newNote : note)
        } else newNotes = [...datas.notes, newNote]
        // create new array
      } else newNotes = [newNote]

      chrome.storage.sync.set({ "notes": newNotes }, function () {
        if (chrome.runtime.error) {
          console.log("Runtime error.")
        }
      })
    }
  })
}

// Get
document.getElementById("get").onclick = function () {
  refreshNotes()
}

// Reset
document.getElementById("reset").onclick = function () {
  chrome.storage.sync.set({ "notes": [] }, function () {
    refreshNotes()
  })
}

// Get notes and refresh the popup
function refreshNotes() {
  chrome.storage.sync.get("notes", function (datas) {
    console.log(datas.notes)
    let notesDiv = document.getElementById("notes")
    if (!chrome.runtime.error) {
      if (datas.notes) {
        // Remove everything that is currently displayed
        while (notesDiv.firstChild) notesDiv.removeChild(notesDiv.firstChild)
        datas.notes.forEach(item => {
          let nameDiv = document.createElement("div")
          let contentDiv = document.createElement("div")

          nameDiv.innerHTML = "<b>" + item.name + "</b>"
          nameDiv.onclick = function () {
            document.getElementById("name").value = item.name
            document.getElementById("content").value = item.content
          }

          contentDiv.innerText = item.content
          notesDiv.appendChild(nameDiv)
          notesDiv.appendChild(contentDiv)
        })
      } else notesDiv.innerText = "Pas de notes !"
    }
  })
}
